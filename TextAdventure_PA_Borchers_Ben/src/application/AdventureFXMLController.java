package application;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.util.Duration;

public class AdventureFXMLController {

    @FXML
    private TextArea textArea;

    @FXML
    private TextField textInput;

    @FXML
    private Button enterBtn;

    @FXML
    private Button btnOne;

    @FXML
    private Button btnTwo;

    @FXML
    private Button btnThree;

    @FXML
    private Button btnFour;

    @FXML
    private Label labelVitality;

    @FXML
    private MenuButton inventory;				// MenuButton als Inventar
    
    private Player player;						// Klasse für Spielerdaten wie Name, Position auf Insel, Vitalität
	private TextOutput textOutput;				// Klasse für die Textausgabe. Sorgt für die Ausgabe der Texte Zeichen für Zeichen, Ausgabegeschwindigkeit lässt sich steuern
	private HashMap<String, MapTile> islandMap;	// die Insel: Sammlung aller Inselkacheln (MapTiles) in einem Dictionary/Map
	private HashMap<String, String> li­b­ret­to;	// Textbuch, das alle Texte für die Textausgabe über textArea beinhaltet und ggf. eine Übersetzung in andere Sprachen erleichtert ;)
	private Timeline updateLabel;				// 
		    
	
	//////////////////// START initialize() /////////////////////////
    public void initialize() {
    	// Bei Start/Neustart/init alle Elemente bis auf TextArea ausblenden 
    	
    	MenuItem empty = new MenuItem("- hier ist nix drin -");			// MenuItem erzeugen ohne onAction; soll im MenuButton angezeigt werden, wenn er quasi leer ist
    	inventory.getItems().add(empty);								// MenuItem dem MenuButton hinzufügen; 
    	
    	// alle Elemente der GUI ausblenden außer Textarea
    	inventory.setVisible(false);
    	textInput.setVisible(false);
    	enterBtn.setVisible(false);
    	labelVitality.setVisible(false);
    	hideAllBtns();
    	    	
    	// Dictionary/HashMap erzeugen und mit Texten fuellen;
    	li­b­ret­to = new HashMap<String, String> ();
    	//libretto.put(String key, String value);
    	li­b­ret­to.put("welcome", "Hey du! - Ja, du!\nHast du Lust auf ein kleines Abenteuer?\nDann klicke unten auf \"Ja\".");
    	li­b­ret­to.put("askForName", "Toll, du hast den Button gefunden. An dieser Stelle wirst du je nach Situation weitere Buttons und Informationen finden.\nWie heisst du denn?\nGib deinen Namen unten in das Feld ein.");
    	li­b­ret­to.put("askForNamePromtText", "Namen eingeben ...");
    	li­b­ret­to.put("hello", "Hallo {NAME}!\n");
    	li­b­ret­to.put("exit", "Oh, schade!\nReisende soll man nicht aufhalten. Du weisst, wo die Tuer ist bzw. wie Du das Anwendung schliessen kannst.\nCiao. :/");
    	li­b­ret­to.put("briefing", "Da du dich nun vorgestellt hast, bin ich wohl an der Reihe. Ich bin deine innere Stimme, dein Gewissen. Ich bin das, was dich, seit dem du denken kannst, antreibt. ;)\nSo wollen wir es auch bei diesem Abenteuer handhaben. Also los!\n\nDu bist im Auftrag der Firma cimdata auf dem Weg zur Java Digital Developer Conference. Du betrittst gerade die Eingangshalle des gerade eroeffneten Flughafens Berlin Brandenburg. Und da ertoent auch schon dein Aufruf! Wieder mal bist du zu spaet. Du nimmst die Beine in die Hand und rennst zu deinem Gate. ...");
    	li­b­ret­to.put("onAirplane", "Mit einem schnippigen Kommentar laesst dich die Stewardess an Bord des Fliegers. Erschoepft faellst du in deinen Sitz.\nDu haettest nicht wieder bis spaet in die Nacht programmieren sollen. Deine Vitalwerte sehen nicht gut aus. Deine Vitalitaet sollte eigentlich annaehernd 100 Prozent betragen, wenn du fit waerst. Du bist allerdings alles andere als fit, {NAME}! :P");
    	li­b­ret­to.put("nameNeeded", "Gib mir einen Namen! Irgendeinen Namen.\nTrage ihn unten in das Feld ein.");
    	li­b­ret­to.put("orderDrink", "Dein Gesundheitszustand scheint dir keine Sorgen zu bereiten? - Oder warum orderst du jetzt einen Drink? - Deine Entscheidung, {NAME}.");
    	li­b­ret­to.put("haveDrink", "war doch klar!\nNa, dann einen guten Flug.");
    	li­b­ret­to.put("fallInSleep", "\n ...                     \n    z z z Z Z Z              \n       z z z Z Z Z              \n          z z z Z Z Z Z Z Z Z Z Z Z Z Z Z Z Z Z Z Z Z Z Z Z Z Z Z Z");
    	li­b­ret­to.put("bang"," B A N G");
    	li­b­ret­to.put("blackout","Je wirst du aus dem Schlaf gerissen. Kreischende Menschen und ohrenbetaeubender Turbinenlaerm lassen dich aufschrecken. Das Flugzeug befindet sich kopfueber in steilem Sinkflug. Du wirst kraeftig durchgeschuettelt. Bevor du dir klar wirst, in welcher Situation du dich befindest, schlaegst du mit Kopf hart an die Kabinenwand.\n\nBLACKOUT!");
    	li­b­ret­to.put("landed", "Hey! Wer hat das Licht ausgemacht? Ohje, {NAME}, geht es dir gut? - Und was rauscht denn hier so?\n{NAME}, mach die Augen auf!");
    	li­b­ret­to.put("stillSleeping", "Na, dann goenn dir noch eine Muetze Schlaf. Ich koennte bei dem Rauschen ja nicht ruhig liegen bleiben.\nDu erinnerst dich hoffentlich, dass du eben noch in einem Flugzeug gesessen hast, oder?\nHallo? {NAME}?");
    	li­b­ret­to.put("openEyes", "Benommen oeffnest du langsam die Augen. Du siehst nur verschwommene Bilder und dein Kopf schmerzt. Du hast Muehe aufzustehen, da der Boden immer wieder nachgibt.\nLangsam werden die Bilder klarer. Das Rauschen kommt von der Brandung. Du bist an einem Strand. Du hast den Absturz ueberlebt, HURRA! - Aber dennoch geht es dir nicht gut, {NAME}!\nDu greifst in die Tasche deiner zerfetzten Hose und holst ein Smartphone heraus. Du drueckst wie wild auf den Home-Button, aber mehr als ein Due-De-Le-Dueueueueiiieee gibt das Geraet nicht von sich. Klarer Fall: Wasserschaden!\nDu faehrst SUV und wohnst in einer Grossstadt, hast aber kein Outdoor-Handy? Was stimmt mit dir nicht? EGAL!!1!!!11 Damit rufst du jedenfalls keine Hilfe mehr. Du wirst dich wohl umsehen muessen. Vielleicht gibt es in der Naehe ein Dorf mit Telefonzelle. ;) \nDann mal los!");
    	li­b­ret­to.put("ocean_1", "Ich sag mal so: Ende im Gelaende! Oder schaffst du es, in deiner Verfassung einen Ozean zu durchqueren?\nNimm einen anderen Weg!");
    	li­b­ret­to.put("ocean_2", "Soweit ich mich erinnere, hast du nur das Seepferdchen. Ist das ueberhaupt ein richtiges Schwimmabzeichen?\nDu solltest besser auf dem Trockenen bleiben und eine andere Richtung erkunden.");
    	li­b­ret­to.put("ocean_3", "Bis zum Horizont nur Wasser und du kannst es nicht einmal trinken. Aetzend!\nSuche im Dschungel nach Wasser.");
    	li­b­ret­to.put("ocean_4", "Das ist nicht die richtige Zeit zum Baden. Du hast weder Badehose noch deine Schwimmfluegel dabei.\nWandern ist dein Sport! Aber in eine andere Richtung, bitte. ;)");
    	li­b­ret­to.put("ocean_5", "Noch ein Schritt weiter und du holst dir nasse Fuesse. Dreh um!");
    	li­b­ret­to.put("ocean_6", "Nichts als Meer! Wie langweilig. Ich glaube, dahinten eine Doenerbude gesehen zu haben. Ach, ich halluziniere. Es war wohl nur ein Doenertier. Hinterher!");
    	li­b­ret­to.put("ocean_7", "Wasser, Wasser und Wasser wohin man schaut. Probier doch mal eine andere Richtung!");
    	li­b­ret­to.put("ocean_8", "Ist das eine Delphinflosse oder ist das ein Hai? An deiner Stelle wuerde ich keinen Fuss ins Wasser setzen. Andere Richtung?");
    	li­b­ret­to.put("ocean_9", "Ich weiss, die Situation scheint aussichtslos, aber kein Grund sich ins Meer zu stuerzen. Kopf hoch! Dahinten sieht es gruen aus. Wo Pflanzen sind, kann etwas Essbares nicht weit sein. Los!");
    	li­b­ret­to.put("newPos", "Du befindest dich jetzt");
    	li­b­ret­to.put("earthquake", "Was ist das? Die Erde unter deinen Fuessen bebt! Die gesamte Insel wird kraeftig durchgeschuettelt.\nSieh es positiv: Hier oben kann dir wenigstens nichts auf den Kopf fallen. ;)");
    	li­b­ret­to.put("afterEarthquake", "Durch das Erdbeben haben sich die Felsbrocken vor dem Hoehleneingang verschoben. Traust du dich rein?");
    	li­b­ret­to.put("inCave", "Es ist so dunkel. Es ist nicht zu erkennen. In Filmen wuerde jetzt hier zufaellig eine Fackel an der Wand haengen oder eine Taschenlampe am Boden liegen. HALT! STOPP! Was blinkt da? ...");
    	li­b­ret­to.put("coward", "Feigling!");
    	li­b­ret­to.put("theEnd", "Es ist ein altes Mobiltelefon und es scheint noch zu funktionieren. Ruf deine Mutter an! Okay, bloede Idee. Pizza-Lieferservice? Call-Girl-Boy-Divers? Egal, nur beeil dich! Wer weiss, wie lang der Akku noch haelt. ...\n\nWaehrend du noch ueberlegst, wen du anrufst, beginnt das Telefon zu klingeln. Vor Schreck faellt es dir beinahe herunter. Du nimmst das Gespraech an und haeltst das Telefon ans Ohr. \"Ja?\" Eine nette Stimme, weisst dich darauf hin, dass es Zeit wird auszusteigen. \"Wie? Was? He?\" - Du kommst zu dir. Du sitzt immer noch Flugzeug und die Stewardess, die dich mit einem schnippigen Kommentar an Bord begruesst hat, erklaert dir das der Flieger vor einer halben Stunde gelandet ist und du doch nun bitte aussteigen solltest ...");
    	li­b­ret­to.put("FIN", "ENDE ...");
    	li­b­ret­to.put("bagIsFull", "Die Tuete ist voll. Wenn du noch mehr in die alte Tuete packst, dann geht sie kaputt. Also lass das!");
    	li­b­ret­to.put("gameOver", "Das war's! Siehst du das weisse Licht, {NAME}? Dann geh darauf zu!\nAdieu, mein Freund. :(\n\n    GAME OVER");
    	    	
    	////////////////////////////////////////////////////////////////////
    	// Feature erzeugen (Food, Activity usw.)
    	// Feature(String text, String type, int vitalityValue, String txtBeforeUse, String txtAfterUse)
    	Feature apple = new Feature("Apfel", "food", 10,"Was haengt da am Baum? Ein Apfel!", "Der Apfel war lecker, was?");
    	Feature banana_1 = new Feature("Banane", "food", 12, "Du hast eine Banane gefunden.", "Du haettest die Schale ruhig abmachen koennen, bevor du die Banane verschlungen hast." );
    	Feature banana_2 = new Feature("Banane", "food", 12, "Du hast eine Banane gefunden.", "Du haettest die Schale ruhig abmachen koennen, bevor du die Banane verschlungen hast." );
    	Feature papaya_1 = new Feature("Papaya", "food", 15, "Da liegt eine Papaya auf dem Boden. Schnapp sie dir!", "Schling nicht so! Hier ist doch niemand, der dir etwas wegnehmen koennte, oder?");
    	Feature papaya_2 = new Feature("Papaya", "food", 15, "Da liegt eine Papaya auf dem Boden. Schnapp sie dir!", "Schling nicht so! Hier ist doch niemand, der dir etwas wegnehmen koennte, oder?");
    	Feature spring = new Feature("Wasser", "drink", 5, "Du hast eine Quelle mit Trinkwasser entdeckt. Trink was!", "Das war erfrischend.");
    	
    	Feature mountainClimbing = new Feature("Berg", "mountain", -20, "Wie hoch der Berg wohl ist?" ,"PUH, das war anstrengend. Dafuer ist die Aussicht atemberaubend schoen.");
    	Feature intoCave = new Feature("Hoehle", "cave", -10, "Allerdings ist der Eingang von grossen Felsbrocken versperrt." ,"Wo ist der Lichtschalter? Pass bloss auf, wo du hintrittst.");
    	
    	Feature bag = new Feature("Plastiktuete", "bag" , 0, "Schau, da hat das Meer was Feines angespuelt.", "In so eine Tuete passt bestimmt Einiges rein und damit kann man prima Sachen transportieren, was meinst du?"); // MenuButton?
    	//inventory.getItems().add(papaya); // TEST
    	
    	////////////////////////////////////////////////////////////////////
    	// Karte der Insel erstellen mit Klasse MapTile
    	// 1. Map erzeugen
    	islandMap = new HashMap<String, MapTile> ();
    	// 2. MapTiles erzeugen OHNE benachbarte MapTiles, da ein MapTile einem anderen erst uebergeben werden kann, wenn es bereits erzeugt wurde
    	// MapTile(String id, String name, boolean isVisited,  // Food food)
    	//                     MapTile( id,     name,  isVisited,  //    food)
    	islandMap.put("A", new MapTile("A", "am Strand", false));
    	islandMap.put("B", new MapTile("B", "am Strand", false));
    	islandMap.put("C", new MapTile("C", "vor einer Hoehle", false, intoCave));
    	islandMap.put("D", new MapTile("D", "am Strand", false));
    	islandMap.put("E", new MapTile("E", "am Strand", false));
    	islandMap.put("F", new MapTile("F", "am Strand", false));
    	islandMap.put("G", new MapTile("G", "am Strand", false));
    	islandMap.put("H", new MapTile("H", "am Strand", false));
    	islandMap.put("I", new MapTile("I", "im Dschungel", false, papaya_1));
    	islandMap.put("J", new MapTile("J", "im Dschungel", false, spring));
    	islandMap.put("K", new MapTile("K", "im Dschungel", false));
    	islandMap.put("L", new MapTile("L", "am Strand", false));
    	islandMap.put("M", new MapTile("M", "am Strand", false, bag));
    	islandMap.put("N", new MapTile("N", "im Dschungel", false));
    	islandMap.put("O", new MapTile("O", "am Fusse eines Berges", false, mountainClimbing));
    	islandMap.put("P", new MapTile("P", "im Dschungel", false, apple));
    	islandMap.put("Q", new MapTile("Q", "am Strand", false));
    	islandMap.put("R", new MapTile("R", "am Strand", false));
    	islandMap.put("S", new MapTile("S", "im Dschungel", false, papaya_2));
    	islandMap.put("T", new MapTile("T", "im Dschungel", false));
    	islandMap.put("U", new MapTile("U", "im Dschungel", false, banana_1));
    	islandMap.put("V", new MapTile("V", "am Strand", false));
    	islandMap.put("W", new MapTile("W", "am Strand", false));
    	islandMap.put("X", new MapTile("X", "im Dschungel", false, banana_2));
    	islandMap.put("Y", new MapTile("Y", "am Strand", false));
    	islandMap.put("Z", new MapTile("Z", "am Strand", false));
    	// 3. benachbarte MapTiles uebergeben
    	//islandMap.get(key).setNeighbourMapTiles(north, east, south, west);
    	islandMap.get("A").setNeighbourMapTiles(null, islandMap.get("B"), islandMap.get("D"), null);
    	islandMap.get("B").setNeighbourMapTiles(null, islandMap.get("C"), null, islandMap.get("A"));
    	islandMap.get("C").setNeighbourMapTiles(null, null, null, islandMap.get("B"));
    	islandMap.get("D").setNeighbourMapTiles(islandMap.get("A"), null, islandMap.get("E"), null);
    	islandMap.get("E").setNeighbourMapTiles(islandMap.get("D"), islandMap.get("F"), islandMap.get("I"), null);
    	islandMap.get("F").setNeighbourMapTiles(null, islandMap.get("G"), islandMap.get("J"), islandMap.get("E"));
    	islandMap.get("G").setNeighbourMapTiles(null, null, islandMap.get("K"), islandMap.get("F"));
    	islandMap.get("H").setNeighbourMapTiles(null, islandMap.get("I"), islandMap.get("M"), null);
    	islandMap.get("I").setNeighbourMapTiles(islandMap.get("E"), islandMap.get("J"), islandMap.get("N"), islandMap.get("H"));
    	islandMap.get("J").setNeighbourMapTiles(islandMap.get("F"), islandMap.get("K"), islandMap.get("O"), islandMap.get("I"));
    	islandMap.get("K").setNeighbourMapTiles(islandMap.get("G"), islandMap.get("L"), islandMap.get("P"), islandMap.get("J"));
    	islandMap.get("L").setNeighbourMapTiles(null, null, islandMap.get("Q"), islandMap.get("K"));
    	islandMap.get("M").setNeighbourMapTiles(islandMap.get("H"), islandMap.get("N"), islandMap.get("R"), null);
    	islandMap.get("N").setNeighbourMapTiles(islandMap.get("I"), islandMap.get("O"), islandMap.get("S"), islandMap.get("M"));
    	islandMap.get("O").setNeighbourMapTiles(islandMap.get("J"), islandMap.get("P"), islandMap.get("T"), islandMap.get("N"));
    	islandMap.get("P").setNeighbourMapTiles(islandMap.get("K"), islandMap.get("Q"), islandMap.get("U"), islandMap.get("O"));
    	islandMap.get("Q").setNeighbourMapTiles(islandMap.get("L"), null, islandMap.get("V"), islandMap.get("P"));
    	islandMap.get("R").setNeighbourMapTiles(islandMap.get("M"), islandMap.get("S"), null, null);
    	islandMap.get("S").setNeighbourMapTiles(islandMap.get("N"), islandMap.get("T"), islandMap.get("W"), islandMap.get("R"));
    	islandMap.get("T").setNeighbourMapTiles(islandMap.get("O"), islandMap.get("U"), islandMap.get("X"), islandMap.get("S"));
    	islandMap.get("U").setNeighbourMapTiles(islandMap.get("P"), islandMap.get("V"), islandMap.get("Y"), islandMap.get("T"));
    	islandMap.get("V").setNeighbourMapTiles(islandMap.get("Q"), null, null, islandMap.get("U"));
    	islandMap.get("W").setNeighbourMapTiles( islandMap.get("S"), islandMap.get("X"), null, null);
    	islandMap.get("X").setNeighbourMapTiles(islandMap.get("T"), islandMap.get("Y"), islandMap.get("Z"), islandMap.get("W"));
    	islandMap.get("Y").setNeighbourMapTiles(islandMap.get("U"), null, null, islandMap.get("X"));
    	islandMap.get("Z").setNeighbourMapTiles(islandMap.get("X"), null, null, null);
    	//System.out.println("Test-Ausgabe: A_north: " + islandMap.get("Z").south);
    	
    	
    	////////////////////////////////////////////////////////////////////
    	MapTile startPositionPlayer = randomStartMapTile(islandMap, "am Strand");  // Start-MapTile für den Spieler per Zufall auswählen; Start nur am Strand möglich/gewünscht  
    	player = new Player(53, startPositionPlayer); // Startwerte: vitality = 50; wird später im Programmverlauf gesetzt
		player.setCurrentPosition(randomStartMapTile(islandMap, "am Strand"));      
		System.out.println("Deine aktuelle Position: " + player.getCurrentPosition().getId() );
		
		// TextOutput(TextArea txtArea, int speed, String playerName)
    	textOutput = new TextOutput(textArea, 60, player);  // TODO: speed auf ca. 50 ms setzen
    	
    	////////////////////////////////////////////////////////////////////
		///// Game Start  //////////////////////////////////////////////////
    	////////////////////////////////////////////////////////////////////
		// Verzögerung der Textausgabe beim Programmstart einstellen
		int delay = 2000; // in Millisekunden (ms)	TODO: Standardwert ca. 1500 ms
    	// Aufruf der ersten Textausgabe
    	welcome(delay);
    	

    } //////////////////// ENDE initialize() /////////////////////////
 
    
	@FXML
    void goNorth() {	// Methode für Button "nach Norden"; Analog folgen die andern drei Himmelsrichtungen
    	moveOnMap(player.getCurrentPosition().getNorth());
    }

    @FXML
    void goEast() {
    	moveOnMap(player.getCurrentPosition().getEast());
    }

    @FXML
    void goSouth() {
    	moveOnMap(player.getCurrentPosition().getSouth());
    }

    @FXML
    void goWest() {
    	moveOnMap(player.getCurrentPosition().getWest());
    }

	public void moveOnMap(MapTile newPosition) {
		if (newPosition == null) {											// Prüfung, ob für die gewählten Richtung statt eines MapTile "null" vorhanden ist; Rand der Insel bzw. Karte wurde erreicht
    		disableAllBtns();													// aktive Buttons deaktivieren
    		textOutput.putOutTxt(0, li­b­ret­to.get(randomOceanTxt()));			// Textausgabe aufrufen
    		textOutput.getTimeline().setOnFinished(ae -> enableAllBtns()); 		// festlegen, welche Methode nach Edne der Textausgabe ausgeführt wird
    	} else {															// sonst MapTile für die gewählten Richtung übergeben
    		player.setCurrentPosition(newPosition);								// aktuelle Position mit neuer Position überschreiben 
    		System.out.println("Deine aktuelle Position: " + player.getCurrentPosition().getId());
    		onNewPosition();													// mögliche Feature der neuen Position abrufen
        	player.allocateVitality(-1);										// jede Bewegung auf der Karte veringert die Vitalität um 1
    	}
	}
	
	public void welcome(int delay) {										// Ausgabe Begrüßungstext
		// putOutTxt(int delay, String... args)  // delay in ms, 
		textOutput.putOutTxt(delay, li­b­ret­to.get("welcome")); 				// Textausgabe mit entsprechendem Text aus HashMap "libretto"
    	textOutput.getTimeline().setOnFinished(ae -> activateBtnAfterWelcome());  // nächste Aktion nach Textausgabe festlegen
	}
    
	public void activateBtnAfterWelcome() {		// Buttons einblenden und mit onAction bei Klick versehen
    	btnTwo.setText("Ja");
    	btnTwo.setOnAction(ae -> inputName());
    	btnThree.setText("Nein");
    	btnThree.setOnAction(ae -> exit());
    	btnTwo.setVisible(true);
    	btnThree.setVisible(true);
	}
	
    public void inputName() {								// Name abfragen
    	disableAllBtns();										// Buttons deaktivieren
    	textOutput.putOutTxt(0, li­b­ret­to.get("askForName"));	// Textausgabe mit entsprechendem Text aus HashMap "libretto"
    	textOutput.getTimeline().setOnFinished(ae -> {			// nächste Aktion nach Textausgabe festlegen
    		textInput.setVisible(true);							// Textfeld für Texteingabe freischalten, einblenden, aktivieren
    		enterBtn.setVisible(true);							// Bestätigungsbutton für Texteingabe freischalten, einblenden, aktivieren; Texteingabe kann auch mit "Enter" per Tastatur bestätigt werden
    		textInput.setDisable(false);
    		enterBtn.setDisable(false);
    		textInput.setPromptText(li­b­ret­to.get("askForNamePromtText"));  // PromtText für Eingabefeld setzen
        	textInput.setOnAction(event -> receiptInput());		// Buttons mit onAction bei Klick versehen
        	enterBtn.setOnAction(event -> receiptInput());
        	textInput.requestFocus();							// Fokus auf Textfeld setzen; Texteingabe kann direkt erfogen, ohne Klick ins Textfeld
    		});
    }
    
    public void exit() {		
    	textOutput.putOutTxt(0, li­b­ret­to.get("exit"));
    	btnTwo.setText("Ich will doch!");
    	// Alert-Dialog und Programm beenden
    	Alert alert = new Alert(AlertType.CONFIRMATION, null, ButtonType.CANCEL, ButtonType.YES);		// Messagebox(alert) für Bestätigung (Sicherheitsabfrage)
    	alert.setTitle("Spiel beenden");
        alert.setHeaderText("Bist du dir sicher?");
    	btnThree.setText("Spiel beenden");
    	btnThree.setOnAction(ae -> alert.showAndWait()   		// Messagebox(alert) an Button gehängt		
        .filter(response -> response == ButtonType.YES)  		// Antwort abwarten
        .ifPresent(response -> System.exit(1)));   		 		// wenn "yes"-Button gedrueckt wurde, Programm beenden/anwendung schliessen
    }
    
    public void receiptInput() {
    	enterBtn.requestFocus();
    	if (textInput.getText().isEmpty()) {  					// Wenn das Eigabefeld leer ist, dann erneut zur Eingabe auffordern
    		textOutput.putOutTxt(0, li­b­ret­to.get("nameNeeded"));
    	} else {												// wenn Textfeld NICHT leer ist, dann ...
	    	player.setName(textInput.getText());					// ... Text als Spielername speichern 
	//    	System.out.println(player.name);
	    	disableTextInput();										// Textfeld deaktivieren
	    	textOutput.putOutTxt(0, li­b­ret­to.get("hello"), li­b­ret­to.get("briefing"));  // putOutTxt( int delay, String text)
	    	textOutput.getTimeline().setOnFinished(ae -> {			
    			textOutput.putOutTxt(500, li­b­ret­to.get("onAirplane"));		// weitere Textausgabe folgt mit Verzögerung; hier wohl besser eine Bestätigung per Button einbauen TODO
    			textOutput.getTimeline().setOnFinished(ae3 -> {
    				showVitality();							// Label Vitality einblenden usw.
		    		takeDrink(); 							// weiter im "Text"
			    	startUpdatingLabelVitality();			// startet fortlaufendes Update fuer Label "Vitalitaet"
			    	startDecreasingVitality();				// startet Reduzierung der Vitalitaet pro 30 Sekunden um 1
    			});
			});
		
	    }
    }
    
    
    public void showVitality() {					// Label Vitality in der GUI einblenden und per Timeline/Keframes animieren
    	labelVitality.setText("Vitalitaet: " + player.getVitality());
    	labelVitality.setVisible(true);
    	
    	KeyValue keyValueXstart = new KeyValue(labelVitality.scaleXProperty(), 1.0);
    	KeyValue keyValueYstart = new KeyValue(labelVitality.scaleYProperty(), 1.0);
    	KeyValue keyValueXend = new KeyValue(labelVitality.scaleXProperty(), 1.7);
    	KeyValue keyValueYend = new KeyValue(labelVitality.scaleYProperty(), 1.7);

    	KeyFrame keyFrameStart = new KeyFrame(Duration.millis(0), keyValueXstart, keyValueYstart);
    	KeyFrame keyFrameEnd = new KeyFrame(Duration.millis(600), keyValueXend, keyValueYend);
    	
    	Timeline labelAnimation = new Timeline(); // neue Timline erstellen
    	labelAnimation.getKeyFrames().addAll(keyFrameStart, keyFrameEnd);			 
    	labelAnimation.setCycleCount(4); 
    	//labelAnimation.setDelay(Duration.millis(1000));
    	labelAnimation.setAutoReverse(true);
    	labelAnimation.play();
    }
    
    public void startUpdatingLabelVitality() {			// Methode startet Aktualisierung des LabelVitality im Sekundentakt
    	updateLabel = new Timeline(new KeyFrame(		// Per Timeline-Animation 
            Duration.millis(1000),
            ae -> {
            labelVitality.setText("Vitalitaet: " + player.getVitality());
            if (player.getVitality() < 15) {					// wenn Vitalität auf einen kritischen Wert sinkt
        		labelVitality.setStyle("-fx-text-fill: red");		// Text-Farbe: rot
        		showVitality();										// label Animation ausführen
        	} else {											// wenn Vitalität über kritischem Wert liegt  
        		labelVitality.setStyle("-fx-text-fill: black");		// Text-Farbe: schwarz und ohne animation
        	}
            if (player.getVitality() < 1) {					// wenn Vitalität gleich 0, dann GAME OVER
            	updateLabel.stop();							// stoppt label-Aktualisierung 
            	labelVitality.setText("Vitalitaet: 0");		
            	textOutput.getTimeline().stop();			// stoppt aktuelle Textausgabe
            	restart(li­b­ret­to.get("gameOver"));			// Textausgabe mit Möglichkeit zum Restart oder Anwendung schließen
            	return;
            }
            }));
      	updateLabel.setCycleCount(Animation.INDEFINITE);
      	updateLabel.play();
    }
    
    public void startDecreasingVitality() {				// Methode startet eine Reduzierung der Vitalitaet von einenm Punkt (-1) pro 30 Sekunden
    	Timeline decreaseVita = new Timeline(new KeyFrame(
                Duration.seconds(30),
                ae -> player.allocateVitality(-1)));
    	decreaseVita.setCycleCount(Animation.INDEFINITE);
    	decreaseVita.play();
    }
    
    public void takeDrink() {
    	textOutput.putOutTxt(4200, li­b­ret­to.get("orderDrink"));
    	textOutput.getTimeline().setOnFinished(ae -> {
    		btnTwo.setText("Gin Tonic");
    		btnTwo.setOnAction(e -> {
    			sleepWell("Gin Tonic"); 
    			player.allocateVitality(-8);		// Gin Tonic verschlechtert die Vitalitaet
	    		showVitality();						// auf Änderung der Vitalität durch Animation des Labels aufmerksam machen
    			});
    		btnThree.setText("Tomatensaft");
    		btnThree.setOnAction(e -> {
    			sleepWell("Tomatensaft"); 
    			player.allocateVitality(5);			// Tomatensaft verbessert die Vitalitaet
	    		showVitality();						// auf Änderung der Vitalität durch Animation des Labels aufmerksam machen
    			});
    		btnTwo.setVisible(true);
    		btnThree.setVisible(true);
    		enableAllBtns();
    	});    	
    }
    
    public void sleepWell(String drink) {
    	disableAllBtns();
    	textOutput.putOutTxt(0, drink, li­b­ret­to.get("haveDrink"), li­b­ret­to.get("fallInSleep"));
    	textOutput.getTimeline().setOnFinished(ae -> { 
    		Timeline tl = blinkingPoints();
    		tl.setOnFinished(aevent -> bang());
    	});
    }
    
    public Timeline blinkingPoints() {		// Animation von 3 blinkenden Punkten
    	String temp = textOutput.getTextArea().getText();  // Zwischenspeicher fuer den aktuell angezeigten Ausgabetext
    	EventHandler<ActionEvent> eventHandler = e -> textOutput.getTextArea().appendText("."); // ActionEvent über das ein Punkt an den Text im TextArea angehängt wird, wenn er aufgrufen wird
    	KeyFrame point1 = new KeyFrame(Duration.millis(300), ae -> textOutput.getTextArea().setText(temp));
    	KeyFrame point2 = new KeyFrame(Duration.millis(600), eventHandler);
    	KeyFrame point3 = new KeyFrame(Duration.millis(900), eventHandler);
    	KeyFrame point4 = new KeyFrame(Duration.millis(1200), eventHandler);
    	
    	Timeline blinking = new Timeline();
    	blinking.getKeyFrames().addAll(point1, point2, point3, point4);
    	blinking.setCycleCount(3);			 
    	blinking.play();
    	return blinking;
    	
    }
    
    public void bang() {
    	textOutput.getTextArea().clear();			// erst TextArea leeren, da folgende Schriftgrößenaenderung sich auch auf den noch enthaltenen Text auswirken würde
    	textOutput.getTextArea().setStyle("-fx-font-size: 75px");		// Schriftgrößer temporaer groesse einstellen
    	textOutput.putOutTxt(200, li­b­ret­to.get("bang"));
    	textOutput.getTimeline().setOnFinished(ae -> {
    		textOutput.putOutTxt(2000, " ");							// kleiner Twist, da wie oben die folgende Schriftgroessenaenderung sich auch auf den noch enthaltenen Text ausgewirkt hätte 
    		textOutput.getTimeline().setOnFinished(ae1 -> {
				textOutput.getTextArea().setStyle("-fx-font-size: 16px");	// Schriftgrößer zuruecksetzen
				textOutput.putOutTxt(0, li­b­ret­to.get("blackout"));
				player.allocateVitality(-10);	  // Die Turbulenzen fordern ihren Tribut; die Vitalität des Spielers nimmt ab
				textOutput.getTimeline().setOnFinished(ae2 -> {
					Timeline tl = blinkingPoints();
					tl.setOnFinished(e -> landed());
					});
    		});
		});    		
    }
    
    public void landed() {
    	textOutput.putOutTxt(1000, li­b­ret­to.get("landed"));
    	textOutput.getTimeline().setOnFinished(ae1 -> {
    		btnTwo.setText("Augen oeffnen");
    		btnTwo.setOnAction(ae2 -> openEyes());
    		btnThree.setText("weiter schlafen");
    		btnThree.setOnAction(ae3 -> {
    			disableAllBtns();
    			textOutput.putOutTxt(0, li­b­ret­to.get("stillSleeping"));
    			textOutput.getTimeline().setOnFinished(ae4 -> enableAllBtns());
    		});
    		
    		btnTwo.setVisible(true);
    		btnThree.setVisible(true);
    		enableAllBtns();
    	}); 
    }
    
	private void openEyes() {
		disableAllBtns();
    	textOutput.putOutTxt(0, li­b­ret­to.get("openEyes"));
    	textOutput.getTimeline().setOnFinished(ae -> showNaviBtns());
	}

	public void disableTextInput() {	// Eingabefeld und Bestätigungsbutton deaktivieren
		textInput.setDisable(true);
    	enterBtn.setDisable(true);
		textInput.setPromptText(null);
    	textInput.clear();
	}
	
	public void disableAllBtns() {		// alle Buttons mit einem Schlag deaktivieren, aber noch sichtbar lassen
		btnOne.setDisable(true);
		btnTwo.setDisable(true);
		btnThree.setDisable(true);
		btnFour.setDisable(true);
	}
	
	public void hideAllBtns() {			// alle Buttons mit einem Schlag ausblenden
		btnOne.setVisible(false);
		btnTwo.setVisible(false);
		btnThree.setVisible(false);
		btnFour.setVisible(false);
	}
	
	public void enableAllBtns() {		// alle Buttons mit einem Schlag aktivieren
		btnOne.setDisable(false);
		btnTwo.setDisable(false);
		btnThree.setDisable(false);
		btnFour.setDisable(false);
	}
    
	public void showNaviBtns() {		// Methode zur wiederkehrenden Einblendung der Navigationsbuttons
		btnOne.setText("nach Norden");
		btnTwo.setText("nach Osten");
		btnThree.setText("nach Sueden");
		btnFour.setText("nach Westen");
		
		btnOne.setOnAction(e -> goNorth());
		btnTwo.setOnAction(e -> goEast());
		btnThree.setOnAction(e -> goSouth());
		btnFour.setOnAction(e -> goWest());
		
		enableAllBtns();
		
		btnOne.setVisible(true);
		btnTwo.setVisible(true);
		btnThree.setVisible(true);
		btnFour.setVisible(true);
	}
	
	public String randomOceanTxt() {		// per Zufall Text aus vorgegebener Liste auswählen; damit werden bei erreichen des Randes der Insel/Karte unterschiedliche Texte ausgeben – wäre sonst langweilig
		ArrayList<String> tempKeyList = new ArrayList<>();		// ArrayList als Zwischenspeicher erzeugen
		for (String key : li­b­ret­to.keySet()) {					// aus HashMap libretto alle Keys, die mit "ocean" beginnen, in ArrayList speichern 
//			System.out.println(key);
			if(key.startsWith("ocean")) {
				tempKeyList.add(key);
			}
		}
		Random rg = new Random(); 								// Zufallsgenerator erzeugen
		int randomKey = rg.nextInt(tempKeyList.size());			// Zufallszahl anhand der Array-Länge bestimmen
//		System.out.println("lenght: "+ tempKeyList.size() + " random: " + randomKey);
		return tempKeyList.get(randomKey);						// ensprechenden Key mittels Zufallszahl als Index aus ArrayList nehmen und zurückgeben per return 
	}
	
	public void onNewPosition() {					// Methode, die bei betreten eines neuen MapTiles einige Aufgaben abarbeitet 
		disableAllBtns();
		
		if (player.getCurrentPosition().getFeature() == null) {   // keine Feature (Food, Action etc.) vorhanden
		textOutput.putOutTxt(0, li­b­ret­to.get("newPos"), player.getCurrentPosition().getName() + ".");
		textOutput.getTimeline().setOnFinished(ae -> showNaviBtns());
		
		} else {  // Feature vorhanden
			textOutput.putOutTxt(0, li­b­ret­to.get("newPos"), player.getCurrentPosition().getName() + ".\n", player.getCurrentPosition().getFeature().getTxtBeforeUse());
			textOutput.getTimeline().setOnFinished(ae -> useFeature(player.getCurrentPosition().getFeature()));

		}
		// Erst am ENDE wird der Besuch der MapTile quittiert!, um if-statement am Anfang der Methode nicht zu konterkarieren
		player.getCurrentPosition().setVisited();  // Besuchte MapTiles(Inselkacheln) werden spaeter in der GUI dargestellt TODO
	}
	
    private void useFeature(Feature feature) {  // Methode, die entsprechend des übergebenen Features Aufgaben abarbeitet
    	hideAllBtns();
    	enableAllBtns();
    	
    	switch (feature.getType()) {					// switch für Auswahl der passenden Aufgaben (Methoden, Funktionen usw.) je nach Feature-Typ
    	case "drink":											// Feature "Wasser/Quelle": die Quelle kann immer wieder besucht werden; Feature wird NICHT gelöscht
    		btnTwo.setText(feature.getText() + " trinken");		
	    	btnThree.setText("Nicht trinken");
	    	
	    	btnTwo.setOnAction(e -> {
	    		disableAllBtns();
	    		player.allocateVitality(feature.getVitalityValue()); 	// Trinken steigert die Vitalität
	    		showVitality();												// auf Änderung der Vitalität durch Animation des Labels aufmerksam machen
	    		textOutput.putOutTxt(100, feature.getTxtAfterUse());
	    		textOutput.getTimeline().setOnFinished(ae -> showNaviBtns());
	    	});
			btnThree.setOnAction(e -> showNaviBtns());
			
			btnTwo.setVisible(true);
			btnThree.setVisible(true);
    		break;
    		
    	case "food":										// Feature "Essen": Nahrung kann nur einmal aufgenommen/gegessen werden; Feature wird anschließend gelöscht
    		btnTwo.setText(feature.getText() + " essen");
    		btnThree.setText("nicht essen");
    		
    		
    		btnTwo.setOnAction(e -> {
    			disableAllBtns();
	    		player.allocateVitality(feature.getVitalityValue());		// Essen steigert die Vitalität
//	    		player.getCurrentPosition().getFeature().setIsUsed(true);   // Der Apfel usw. kann nur einmal gegessen werden
	    		player.getCurrentPosition().setFeature(null);				// Feature wird gelöscht
	    		showVitality();												// auf Änderung der Vitalität durch Animation des Labels aufmerksam machen
	    		textOutput.putOutTxt(100, feature.getTxtAfterUse());
	    		textOutput.getTimeline().setOnFinished(ae -> showNaviBtns());
	    	});
    		btnThree.setOnAction(e -> showNaviBtns());
    		btnTwo.setVisible(true);
    		btnThree.setVisible(true);
    		
    		if (inventory.isVisible()) {									// Wenn Inventar/Plastiktüte/bag freigeschaltet ist, dann können Features vom Typ Food ins Inventar aufgenommen werden, denn die Klasse Feature hat per Vererbung Eigenschaften von Klasse MenuItem
	    		btnFour.setText("einpacken");
	    		btnFour.setOnAction(e -> {
	    			if (inventory.getItems().size() < 4) {					// wenn Inventar/Plastiktüte/bag kleiner 4 (quasi kleiner 3, da Platzhalter "empty" ausgeblendet wird, sobald ein weiteres MenuItem dem MenuButton hinzugefügt wird)
	    				feature.setOnAction(ae-> {							// feature bekommt Methoden bei Klick (onAction) zugewiesen
	    					player.allocateVitality(feature.getVitalityValue());	
	    					inventory.getItems().remove(feature);			// bei Benutzung des Features im Inventar per Klick, dann Vitalität entsprechend verrechnen und anschließend Feature aus Inventar löschen
	    					ifInventoryEmpty();								// Methode prüft, ob MenüButton quasi leer ist und blendet Platzhalter "empty" ein oder aus
	    				});
	    				inventory.getItems().add(feature);					// Feature dem Inventar hinzufügen
	    				ifInventoryEmpty();									// Methode prüft, ob MenüButton quasi leer ist und blendet Platzhalter "empty" ein oder aus
//	    				System.out.println(feature.getText() + " in Tuete stecken." + inventory.getItems());
	    				player.getCurrentPosition().setFeature(null);		// Feature aus MapTile löschen
//	    				System.out.println(inventory.getItems().size());
	    				showNaviBtns();										// NavigationsButtons erneutr einblenden
	    			} else {											// ins Inventar/Plastiktüte/bag passen nur drei Gegenstände
	    				textOutput.putOutTxt(100, li­b­ret­to.get("bagIsFull"));
	    	    		textOutput.getTimeline().setOnFinished(ae -> useFeature(feature));
	    			}
	    		});
	    		btnFour.setVisible(true);
    		}
       		break;
    		
    	case "mountain":									// Feature "Berg": Berg kann immer wieder bestiegen werden
    		btnTwo.setText(feature.getText() + " besteigen");
	    	btnThree.setText("unten bleiben");
	    	
	    	btnTwo.setOnAction(e -> {
	    		disableAllBtns();
	    		player.allocateVitality(feature.getVitalityValue());		// Berg besteigen verringert die Vitalität
	    		showVitality();												// auf Änderung der Vitalität durch Animation des Labels aufmerksam machen
	    		textOutput.putOutTxt(100, feature.getTxtAfterUse());
	    		textOutput.getTimeline().setOnFinished(ae -> {
	    			if (feature.isUsed() || !islandMap.get("C").isVisited()) {  // Erde bebt erst, wenn die verschlossene Höhle entdeckt wurde (isVisited = true). Das Erdbeben wird nur einmal ausgeführt!
	    				showNaviBtns(); 				
	    			} else {
	    				feature.setIsUsed(true);
	    				islandMap.get("C").getFeature().setIsUsed(true);
	    				islandMap.get("C").getFeature().setTxtBeforeUse(li­b­ret­to.get("afterEarthquake"));
	    				textOutput.putOutTxt(2000, li­b­ret­to.get("earthquake"));
	    				textOutput.getTimeline().setOnFinished(ActionEvent -> showNaviBtns());
	    			}
	    		});
	    	});
			btnThree.setOnAction(e -> showNaviBtns());
			
			btnTwo.setVisible(true);
			btnThree.setVisible(true);
    		break;
    		
    	case "cave":												// Feature "Höhle": Höhle kann erst betreten werden, wenn der Eingang frei ist! Felsen vor Höhleneingan verschiwnden nach Erdbeben, siehe oben Feature Mountain 
    		if (feature.isUsed()) {
	    		btnTwo.setText(feature.getText() + " betreten");
		    	btnThree.setText("draussen bleiben");
		    	
		    	btnTwo.setOnAction(e -> {
		    		disableAllBtns();
		    		player.allocateVitality(feature.getVitalityValue());		// Höhle betreten verringert die Vitalität
		    		showVitality();												// auf Änderung der Vitalität durch Animation des Labels aufmerksam machen
		    		textOutput.putOutTxt(100, feature.getTxtAfterUse());
		    		textOutput.getTimeline().setOnFinished(ae -> inCave());  
		    	});
				btnThree.setOnAction(e -> showNaviBtns());
				
				btnTwo.setVisible(true);
				btnThree.setVisible(true);
    		} else {
    			showNaviBtns();
    		}
    		break;
    		
    	case "bag":								// Feature "Plastiktüte": Beutel kann nur einmal aufgenommen werden; Feature wird anschließend gelöscht
    		btnTwo.setText("Beutel aufheben");
    		btnThree.setText("liegen lassen");
    		
    		btnTwo.setOnAction(e -> {
    			disableAllBtns();
	    		player.getCurrentPosition().setFeature(null);		// Feature/Beutel entfernen/löschen; Der Beutel darf nur einmal gefunden werden
	    		inventory.setVisible(true);							// Beutel/Inventar/MenuButton anzeigen im GUI
	    		ifInventoryEmpty();									// MenuItem "Leer" anzeigen; Beutel ist zu Beginn leer
	    		textOutput.putOutTxt(100, feature.getTxtAfterUse());
	    		textOutput.getTimeline().setOnFinished(ae -> showNaviBtns());
	    	});
    		btnThree.setOnAction(e -> showNaviBtns());
    		
    		btnTwo.setVisible(true);
    		btnThree.setVisible(true);
    		break;
    	}
	}

    
    public void ifInventoryEmpty() {		// Methode prüft, ob MenuButton quasi leer ist und blendet Platzhalter "empty"-MenuItem ein oder aus
		if(inventory.getItems().size() == 1) {
			inventory.getItems().get(0).setVisible(true);		// MenuItem "Leer" anzeigen
		} else {
			inventory.getItems().get(0).setVisible(false);		// MenuItem "Leer" ausblenden
		}
    }
    
    public void inCave() {		// Methode für Aufagen in der Höhle;
    	disableAllBtns();
    	textOutput.putOutTxt(100, li­b­ret­to.get("inCave"));
		textOutput.getTimeline().setOnFinished(ae -> {
			btnTwo.setText("Ding aufheben");
	    	btnThree.setText("ich will raus");
	    	
	    	btnTwo.setOnAction(e -> { // theEnd
	    		disableAllBtns();
	    		textOutput.putOutTxt(100, li­b­ret­to.get("theEnd"));
	    		textOutput.getTimeline().setOnFinished(eve -> restart(li­b­ret­to.get("FIN")));
	    	});
			btnThree.setOnAction(event -> {
				disableAllBtns();
				textOutput.putOutTxt(100, li­b­ret­to.get("coward"));
				showNaviBtns();});
			
			btnTwo.setVisible(true);
			btnThree.setVisible(true);
			enableAllBtns();
		}); 
    	
    }
    
    public void restart(String txt) {		// Methode bietet Möglichkeit zum Neustart und Spiel beenden; Methode wird genutzt bei erfolgreichem Spielende oder Game Over (vitality = 0)
    	disableAllBtns();
    	hideAllBtns();
    	textOutput.getTextArea().clear();
    	textOutput.putOutTxt(100, txt);
    	textOutput.getTimeline().setOnFinished(ae ->{
			btnTwo.setText("Neustart");
	    	btnThree.setText("Spiel beenden");
	    	
	    	btnTwo.setOnAction(ae1 -> {
	        	textOutput.getTextArea().clear();
	    		initialize();		// Neustart; initialize() erneut aufrufen und alles zuruecksetzen
	    		});  
	    	
	    	Alert alert = new Alert(AlertType.CONFIRMATION, null, ButtonType.CANCEL, ButtonType.YES);		// Sicherheitsabfrage bei "Spiel beenden"
	    	alert.setTitle("Spiel beenden");
	        alert.setHeaderText("Bist du dir sicher?");
	    	btnThree.setOnAction(ae2 -> alert.showAndWait()   
	        .filter(response -> response == ButtonType.YES)  // Antwort abwarten
	        .ifPresent(response -> System.exit(1))); 
			
			btnTwo.setVisible(true);
			btnThree.setVisible(true);
			enableAllBtns();
    	});
    }

	public MapTile randomStartMapTile(HashMap<String, MapTile> islandMap, String name) {		// Methode für z. B. zufällige Startposition des Spielers
		ArrayList<MapTile> mapTileList = new ArrayList<MapTile>();								// ArrayList als Zwischenspeicher erstellen
    	for (MapTile tile : islandMap.values()) {												// HashMap islandMap mit allen MapTiles durchlaufen per forEach 
    		if (tile.getName().equals(name)) {													// alle MapTiles, deren "Name" mit dem übergebenen Parameter "name" übereinstimmen, werden im Array abgelegt
    			mapTileList.add(tile);
    			//System.out.println(tile.getName() + " " + tile.getId() );
    		}
    	}
    	Random rg = new Random();
    	//System.out.println(mapTileList.size());
    	int randomIndex = rg.nextInt(mapTileList.size());
    	return mapTileList.get(randomIndex);											// Per Zufallsgenerator MapTile aus Array auswählen und per return zurückgeben
	}

}
