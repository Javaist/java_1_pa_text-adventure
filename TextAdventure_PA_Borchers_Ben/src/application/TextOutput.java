package application;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.control.TextArea;
import javafx.util.Duration;

public class TextOutput {			// Klasse für die Textausgabe mit eigenen Methoden für diese Aufgabe

    private int StringIndex;			// Index/Zähler für die einzelnen Zeichen des Strings
    private int speed;					// Geschwindigkeit der Textausgabe in ms
    private TextArea txtArea;			// Ausgabe für Text
    private Timeline timeline;			// Zeitsteuerung/Zeitschleife für Textausgabe
    private Player player;			// Spielernamen übergeben, um Platzerhalter {NAME} in den Texten mit dem Spielernamen zu ersetzen zu können
   
    
    // Konstruktor
	public TextOutput(TextArea txtArea, int speed, Player player) {
		super();
		this.txtArea = txtArea;
		this.speed = speed;
		this.player = player;
	}

	// Methoden
	public TextArea getTextArea() {
		return txtArea;
	}

	public void setTextArea(TextArea txtArea) {
		this.txtArea = txtArea;
	}


	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}


	/**
	 *  Diese Methode animiert per Timeline die Ausgabe der übergebenen Strings Zeichen für Zeichen.
	 *  Der Start der Ausgabe kann mittels Delay verzögert werden.
	 * <p>
	 * @param delay in ms
	 * @param args
	 */
	public void putOutTxt(int delay, String... args) {
		StringIndex = 0;			// index auf 0 zurücksetzen
		String tempTxt = "";
		for (String text : args) {  // args ist ein Array, das alle übergebenen Strings enthält, die in folgender Schleife  zu einem String verkettet/zusammengeführt werden 
			if (player.getName() != null) {
				tempTxt += text.replace("{NAME}", player.getName());   // Platzhalter {NAME} im String value wird mit playerName (player.getName()) ersetzt, wenn vorhanden
			} else {
				tempTxt += text;
			}
			if (!Character.toString(text.charAt(text.length()-1)).equals("\n")) {  // Ist am Ende des Strings ein Zeilenumbruch "\n", dann soll KEIN Leerzeichen zwischen die Strings gesetzt werden
				tempTxt += " ";
			}
		}
		
		final String textCopy = tempTxt;  // für Übergabe von text an ActionEvent-Lambda
//		System.out.println("textCopy: " + textCopy);
		
		timeline = new Timeline();			// neue Timline erstellen
		// Timeline als Schleife mit Verzögerung nutzen; alle 80 ms wird die Funktion Text aufgerufen; JavaFX akzeptert keine for-Schleife mit sleep()
    	timeline.getKeyFrames().add(new KeyFrame(	
    		Duration.millis(speed),			 
        	ae -> txtByChar(textCopy)));
    	timeline.setDelay(Duration.millis(delay));
    	timeline.setCycleCount(textCopy.length());  // timeline wiederholt sich so oft, wie Zeichen im String "text" stecken, falls sie nicht vorher gestoppt wird.
    	timeline.play();
	}
	
	// Textausgabe: Zeichen für Zeichen
    private void txtByChar(String text) {
    	if (StringIndex == 0) {
    		txtArea.clear();		// TextArea leeren für neue Textausgabe;
    	}
		txtArea.appendText(Character.toString(text.charAt(StringIndex))); // Textausgabe: Zeichen für Zeichen
    	StringIndex++;
    }
    
    public Timeline getTimeline() {
    	return timeline;
    }
}
