package application;
	
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Font;
import javafx.fxml.FXMLLoader;


public class Main extends Application {
	@Override
	public void start(Stage primaryStage) {
		try {
			// GUI per FXML
			BorderPane root = (BorderPane)FXMLLoader.load(getClass().getResource("AdventureFXML.fxml"));
			Scene scene = new Scene(root);
			
			// Schriftart laden, die nicht standardmäßig auf allen OS verfügbar ist
			Font.loadFont(getClass().getResourceAsStream("/pixelmix.ttf"), 16);
			
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setTitle("Ein kleines FXML-Abenteuer");
			primaryStage.setResizable(false);
			primaryStage.setScene(scene);
			primaryStage.show();
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		
		
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
