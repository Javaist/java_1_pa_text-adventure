package application;

public class MapTile {
	
	private String id;
	private String name;
	private boolean isVisited;
	private MapTile north;
	private MapTile east;
	private MapTile south;
	private MapTile west;
	private Feature feature;

	
	public MapTile(String id, String name, boolean isVisited) {
		super();
		this.id = id;
		this.name = name;
		this.isVisited = isVisited;
	}



	public MapTile(String id, String name, boolean isVisited, Feature feature) {
		super();
		this.id = id;
		this.name = name;
		this.isVisited = isVisited;
		this.feature = feature;
	}



	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isVisited() {
		return isVisited;
	}
	/**
	 * Setzt die Variable "isVisited" im MapTile auf TRUE.
	 */
	public void setVisited() {
		this.isVisited = true;
	}
	
	/**
	 * With this set-Method do you set all neighbours at once. If there are no neighbour, then set parameter of "null".
	 * <p>
	 * @param north
	 * @param east
	 * @param south
	 * @param west
	 */
	public void setNeighbourMapTiles(MapTile north, MapTile east, MapTile south, MapTile west) {
		this.north = north;
		this.east = east;
		this.south = south;
		this.west = west;
	}
	
	public MapTile getNorth() {
		return north;
	}

	public void setNorth(MapTile north) {
		this.north = north;
	}

	public MapTile getEast() {
		return east;
	}

	public void setEast(MapTile east) {
		this.east = east;
	}

	public MapTile getSouth() {
		return south;
	}

	public void setSouth(MapTile south) {
		this.south = south;
	}

	public MapTile getWest() {
		return west;
	}

	public void setWest(MapTile west) {
		this.west = west;
	}

	public Feature getFeature() {
		return feature;
	}

	public void setFeature(Feature feature) {
		this.feature = feature;
	}
	

	@Override
	public String toString() {
		return "MapTile [id=" + id + ", name=" + name + ", isVisited=" + isVisited + ", north=" + north + ", east="
				+ east + ", south=" + south + ", west=" + west + ", feature=" + feature + "]";
	}

	
	

}
