package application;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.util.Duration;

public class Player {  // Klasser für den Spieler, die folgende Werte aufnimmt und eine eigenen Methode allocateVitality() besitzt
	
	private String name;			// name ;)
	private int vitality;			// Healthpoints
	private MapTile currentPosition;// aktuelle Position auf der Insel/Karte	

	// Konstruktor
	public Player(String name, int vitalityOnStart) {
		super();
		this.name = name;
		this.vitality = vitalityOnStart;
	}

	public Player(int vitality, MapTile currentPosition) {
		super();
		this.vitality = vitality;
		this.currentPosition = currentPosition;
	}
	
	public Player(String name, int vitality, MapTile currentPosition) {
		super();
		this.name = name;
		this.vitality = vitality;
		this.currentPosition = currentPosition;
	}

	
	// Methoden
	/**
	 * 
	 * Diese Methode verrechnet den übergebenen Nährwert (nutritiveValue)
	 * mit vitality. Dies geschiet per timelin: Pro Sekunde wird vitality 
	 * um 1 erhöht oder verringert, je nach dem, ob Nährwert ein positiv 
	 * oder negativ Vorzeichen besitzt. Die Anzahl der Druchläufe wird vom
	 * Nährwert bestimmt. 
	 *
	 * @param nutritiveValue
	 */
	public void allocateVitality(int nutritiveValue) {
		Timeline timeline = new Timeline(new KeyFrame(
                Duration.millis(2000),
                ae -> {
                	if (nutritiveValue < 0) {
                		this.vitality--;
                	} else {
                		this.vitality++;
                	}
                }));
        timeline.setCycleCount(Math.abs(nutritiveValue));
        timeline.play();

		
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getVitality() {
		return vitality;
	}

	public void setVitality(int vitality) {
		this.vitality = vitality;
	}

	public MapTile getCurrentPosition() {
		return currentPosition;
	}

	public void setCurrentPosition(MapTile currentPosition) {
		this.currentPosition = currentPosition;
	}

}
