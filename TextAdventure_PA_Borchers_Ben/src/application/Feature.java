package application;

import javafx.scene.control.MenuItem;

public class Feature extends MenuItem {  // Vererbung der Eigenschaften von Klasse MenuItem, damit Features von MenuButton aufgenommen und mit Methoden genutzt werden können. 

	private String type;		// Art/ Type des Features 
	private int vitalityValue;  // kann/darf positiv oder negativ sein
	private String txtBeforeUse;
	private String txtAfterUse;
	private boolean isUsed;

	public Feature(String text, String type, int vitalityValue, String txtBeforeUse, String txtAfterUse) {
		super(text);
		this.type = type;
		this.vitalityValue = vitalityValue;
		this.txtBeforeUse = txtBeforeUse;
		this.txtAfterUse = txtAfterUse;
		this.isUsed = false;
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getVitalityValue() {
		return vitalityValue;
	}
	
	public String getTxtBeforeUse() {
		return txtBeforeUse;
	}

	public void setTxtBeforeUse(String txtBeforeUse) {
		this.txtBeforeUse = txtBeforeUse;
	}
	
	public String getTxtAfterUse() {
		return txtAfterUse;
	}
	
	public void setTxtAfterUse(String txtAfterUse) {
		this.txtAfterUse = txtAfterUse;
	}
	
	public boolean isUsed() {
		return isUsed;
	}

	public void setIsUsed(boolean isUsed) {
		this.isUsed = isUsed;
	}

	

}
